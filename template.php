<?php
function fileTo64($fileName)
{
    return base64_encode(file_get_contents($fileName));
}

function sanitize($val)
{
    return htmlentities($val, ENT_QUOTES|ENT_SUBSTITUTE);
}

$generalAbout   = isset($argv[1]) ? $argv[1] : null;
$boardName      = isset($argv[2]) ? $argv[2] : null;
$boardPath      = isset($argv[3]) ? $argv[3] : null;

$boardPng       = "$boardPath/board.png";
$schemaPng      = "$boardPath/schema.png";
$camPng         = "$boardPath/cam.png";
$howToStartPng  = "images/howToStart.png";
$equipmentJpg   = "images/equipment.jpg";

$boardPng64       = "data:image/png;base64,".fileTo64($boardPng);
$schemaPng64      = "data:image/png;base64,".fileTo64($schemaPng);
$camPng64         = "data:image/png;base64,".fileTo64($camPng);
$howToStartPng64  = "data:image/png;base64,".fileTo64($howToStartPng);
$equipmentJpg64   = "data:image/jpg;base64,".fileTo64($equipmentJpg);

$boardAbout     = "$boardPath/about.txt";
$boardRating    = "$boardPath/rating.csv";
$boardCircuit   = "$boardPath/circute.txt";
$boardBom       = "$boardPath/bom.csv";
$disclaimer     = "disclaimer.txt";
$license        = "license.txt";
$camRar         = "$boardPath/cam.rar";
$designZip      = "$boardPath/design.zip";





?>
<!DOCTYPE html>
<html>
<title><?php echo $boardName ?></title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="data:text/css;base64,<?php echo base64_encode(file_get_contents('https://www.w3schools.com/w3css/4/w3.css')); ?>">
<link rel="stylesheet" href="data:text/css;base64,<?php echo base64_encode(file_get_contents('https://fonts.googleapis.com/css?family=Lato')); ?>">
<link rel="stylesheet" href="data:text/css;base64,<?php echo base64_encode(file_get_contents('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="data:text/javascript;base64,<?php echo base64_encode(file_get_contents('https://code.jquery.com/jquery-3.2.1.slim.min.js')); ?>" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="data:text/javascript;base64,<?php echo base64_encode(file_get_contents('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js')); ?>" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<link rel="stylesheet" href="data:text/css;base64,<?php echo base64_encode(file_get_contents('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css')); ?>"  type="text/css" crossorigin="anonymous">
<script src="data:text/javascript;base64,<?php echo base64_encode(file_get_contents('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js')); ?>" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
<style>
body,
h1,
h2,
h3,
h4,
h5,
h6 {
    font-family: "Lato", sans-serif;
}

body,
html {
    height: 100%;
    color: #777;
    line-height: 1.8;
}

/* Create a Parallax Effect */

.bgimg-1,
.bgimg-2,
.bgimg-3,
.bgimg-4,
.bgimg-5,
.bgimg-6 {
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}

/* First image (Logo. Full height) */

.bgimg-1 {
    background-image: url('<?php echo $boardPng64 ?>');
    min-height: 100%;
}


/* Second image (SCHEMA) */

.bgimg-2 {
    background-image: url('<?php echo $schemaPng64 ?>');
    min-height: 400px;
}

/* Third image (LAYOUT) */

.bgimg-3 {
    background-image: url('<?php echo $camPng64 ?>');
    min-height: 400px;
}

/* BOM */

.bgimg-4 {

    background-image: url('<?php echo $howToStartPng64 ?>');
    min-height: 400px;
}

/* LINKS */

.bgimg-5 {

    background-image: url('<?php echo $equipmentJpg64 ?>');
    min-height: 400px;
}

.bgimg-6 {
    background-image: url('<?php echo $schemaPng64 ?>');
    min-height: 400px;
}

.w3-wide {
    letter-spacing: 10px;
}

.w3-hover-opacity {
    cursor: pointer;
}

.flex-container {
    display: flex;
}




/* Turn off parallax scrolling for tablets and phones */

@media only screen and (max-device-width: 1024px) {
    .bgimg-1,
    .bgimg-2,
    .bgimg-3,
    .bgimg-4,
    .bgimg-5,
    .bgimg-6 {
        background-attachment: scroll;
    }
}
</style>

<body>
    <!-- Navbar (sit on top) -->
    <div class="w3-top">
        <div class="w3-bar" id="myNavbar">
            <a class="w3-bar-item w3-button w3-hover-black w3-hide-medium w3-hide-large w3-right" href="javascript:void(0);" onclick="toggleFunction()" title="Toggle Navigation Menu">
            <i class="fa fa-bars"></i>
        </a>
            <a href="#home" class="w3-bar-item w3-button">HOME</a>
            <a href="#about" class="w3-bar-item w3-button w3-hide-small"><i class="fa fa-user"></i>ABOUT</a>
            <a href="#schemaAndLayout" class="w3-bar-item w3-button w3-hide-small"><i class="fa fa-th"></i>SCHEMA &#38; LAYOUT</a>
            <a href="#bom" class="w3-bar-item w3-button w3-hide-small"><i class="fa fa-th"></i>BOM</a>
            <a href="#howToStart" class="w3-bar-item w3-button w3-hide-small"><i class="fa fa-th"></i>HOW TO START</a>
            <a href="#equipment" class="w3-bar-item w3-button w3-hide-small"><i class="fa fa-th"></i>EQUIPMENT</a>
            
            <a href="#contact" class="w3-bar-item w3-button w3-hide-small"><i class="fa fa-envelope"></i>CONTACT</a>
            </a>
        </div>
        <!-- Navbar on small screens -->
        <div id="navDemo" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium">
            <a href="#about" class="w3-bar-item w3-button" onclick="toggleFunction()">ABOUT</a>
            <a href="#schemaAndLayout" class="w3-bar-item w3-button" onclick="toggleFunction()">SCHEMA &#38; LAYOUT</a>
            <a href="#bom" class="w3-bar-item w3-button" onclick="toggleFunction()">BOM</a>
            <a href="#howToStart" class="w3-bar-item w3-button" onclick="toggleFunction()">HOW TO START</a>
            <a href="#equipment" class="w3-bar-item w3-button" onclick="toggleFunction()">EQUIPMENT</a>
            <a href="#contact" class="w3-bar-item w3-button" onclick="toggleFunction()">CONTACT</a>
        </div>
    </div>
    <!-- First Parallax Image with Logo Text -->
    <div class="bgimg-1 w3-display-container w3-opacity-min" id="home">
        <div class="w3-display-middle" style="white-space:nowrap;">
            <span class="w3-center w3-padding-large w3-black w3-xlarge w3-wide w3-animate-opacity">5V&#38;3V <span class="w3-hide-small">POWER SUPPLY</span> BOARD</span>
        </div>
    </div>
    <!-- Container (About Section) -->
    <div class="w3-content w3-container w3-padding-64" id="about">
        <h3 class="w3-center">ABOUT</h3>

        <p class="w3-center"><em>The Project</em></p>
        <p><?php echo htmlentities(file_get_contents($generalAbout), ENT_QUOTES|ENT_SUBSTITUTE);?>
        </p>
        <br>
        <h4 class="w3-center">The Board</h4>
        <div class="w3-row">
            <div class="w3-col m6 w3-center w3-padding-large">
                <p><b><i class="fa fa-user w3-margin-right"></i>Power supply board</b></p>
                <br>
                <img src="<?php echo $boardPng64 ?>" class="w3-round w3-image w3-opacity w3-hover-opacity-off" alt="Photo of Me" width="500" height="333">
            </div>
            <!-- Hide this text on small devices -->
            <div class="w3-col m6 w3-hide-small w3-padding-large">
                <p><?php echo htmlentities(file_get_contents($boardAbout), ENT_QUOTES|ENT_SUBSTITUTE);?>
                </p>
            </div>
        </div>
        <p class="w3-large w3-center w3-padding-16">Rating</p>
        <?php
        $handle = fopen($boardRating, "r");
        while ($row = fgetcsv($handle)) { ?>
            <div class="row">
                <div class="col"></div>
                <div class="col border w3-center w3-padding-large w3-dark-grey"><?php echo htmlentities($row[0], ENT_QUOTES|ENT_SUBSTITUTE); ?></div>
                <div class="col border w3-center w3-padding-large w3-light-grey"><?php echo htmlentities($row[1], ENT_QUOTES|ENT_SUBSTITUTE); ?></div>
                <div class="col"></div>
            </div>
        <?php
        }
        fclose($handle);
        ?>
    </div>
    <div class="w3-row w3-center w3-dark-grey w3-padding-16">
     
    </div>
    <!-- Second Parallax Image with Portfolio Text -->
    <div class="bgimg-2 w3-display-container w3-opacity-min">
        <div class="w3-display-middle">
            <span class="w3-xxlarge w3-text-white w3-wide">SCHEMA &#38; LAYOUT</span>
        </div>
    </div>
    <!-- Container (Portfolio Section) -->
    <div class="w3-content w3-container w3-padding-64" id="schemaAndLayout">
        <h3 class="w3-center">THE CIRCUTE</h3>
        <p class="w3-center"><em>Here we have the circuit schema and PCB<br> Click on the images to make them bigger</em></p>
        <br>
        <!-- Responsive Grid. Four columns on tablets, laptops and desktops. Will stack on mobile devices/small screens (100% width) -->
        <div class="w3-row-padding w3-center">
            <div class="w3-col m6">
                <img src="<?php echo $camPng64 ?>" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity w3-circle w3-border w3-padding" alt="Board Layout">
            </div>
            <div class="w3-col m6">
                <img src="<?php echo $schemaPng64 ?>" style="width:100%" onclick="onClick(this)" class="w3-hover-opacity w3-circle w3-border w3-padding" alt="Circute schema">
            </div>
        </div>
        <p> 
            <?php echo htmlentities(file_get_contents($boardCircuit), ENT_QUOTES|ENT_SUBSTITUTE);?> 
        </p>
        <div class="w3-center">
            <a download='design.zip' href="data:application/zip;base64,<?php echo base64_encode(file_get_contents($designZip));?>" " class="w3-button w3-black">Download Eagle schema and board files</a>
        </div>
    </div>
    <!-- Modal for full size images on click-->
    <div id="modal01" class="w3-modal w3-black" onclick="this.style.display='none'">
        <span class="w3-button w3-large w3-black w3-display-topright" title="Close Modal Image"><i class="fa fa-remove"></i></span>
        <div class="w3-modal-content w3-animate-zoom w3-center w3-transparent w3-padding-64">
            <img id="img01" class="w3-image">
            <p id="caption" class="w3-opacity w3-large"></p>
        </div>
    </div>
    <!-- Modal for full size text on click-->
    <div id="modal02" class="w3-modal w3-black" onclick="this.style.display='none'">
        <span class="w3-button w3-large w3-black w3-display-topright" title="Close Modal Image"><i class="fa fa-remove"></i></span>
        <div class="w3-modal-content w3-animate-zoom w3-center w3-transparent w3-padding-64">
            <h5 id="headline01"> </h5>
            <p id="text01" style="white-space: pre-wrap;" class="w3-left-align w3-opacity w3-large"></p>
        </div>
    </div>
    <!-- Third Parallax Image with Portfolio Text -->
    <div class="bgimg-3 w3-display-container w3-opacity-min">
        <div class="w3-display-middle">
            <span class="w3-xxlarge w3-text-white w3-wide">BOM</span>
        </div>
    </div>
    <!-- Container (Contact Section) -->
    <?php
        
    ?>
    <div class="w3-content w3-container w3-padding-64" id="bom">
        <p>BOM stand for Bill of Materials, in other words, what are the needed parts for this board </p>
        <?php
        $handle = fopen($boardBom, "r");
        $header = fgetcsv($handle);
        while ($row = fgetcsv($handle)) {
            ?>
            <div class="media border w3-padding">
                <img class="align-self-center mr-3" style="height: 128px;width: 128px" src="<?php echo  "data:image/jpg;base64," . base64_encode(file_get_contents("images/".$row[3].".jpg")); ?>" alt="Geneplaceholder image">
                <div class="media-body ">
                    <h5 class="mt-0"><?php echo $row[2]; ?></h5>
                    <div class="row ">
                        <div class="col-6">
                            <div class="flex-container border">
                                <div class="w3-padding-small w3-dark-grey" style="width:40%"><?php echo sanitize($header[0]);?></div>
                                <div class="w3-padding-small w3-light-grey" style="width:60%"><?php echo sanitize($row[0]); ?></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="flex-container border">
                                <div class="w3-padding-small w3-dark-grey" style="width:40%"><?php echo sanitize($header[5]);
                                ;?></div>
                                <div class="w3-padding-small w3-light-grey" style="width:60%"><?php echo sanitize($row[5]); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="flex-container border">
                                <div class="w3-padding-small w3-dark-grey" style="width:40%"><?php echo sanitize($header[1]);?></div>
                                <div class="w3-padding-small w3-light-grey" style="width:60%"><?php echo sanitize($row[1]); ?></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="flex-container border">
                                <div class="w3-padding-small w3-dark-grey" style="width:40%"><?php echo sanitize($header[4]);
                                ;?></div>
                                <div class="w3-padding-small w3-light-grey" style="width:60%"><?php echo sanitize($row[4]); ?></div>
                            </div>
                        </div>   
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="flex-container border">
                                <div class="w3-padding-small w3-dark-grey" style="width:40%"><?php echo sanitize($header[6]);
                                ;?></div>
                                <div class="w3-padding-small w3-light-grey" style="width:60%"><?php echo sanitize($row[6]); ?></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="flex-container border">
                                <div class="w3-padding-small w3-dark-grey" style="width:40%"><?php echo sanitize($header[7]);?></div>
                                <div class="w3-padding-small w3-light-grey" style="width:60%"><?php echo sanitize($row[7]); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="flex-container border">
                                <div class="w3-padding-small w3-dark-grey" style="width:40%">Buy</div>
                                <div class="w3-padding-small w3-light-grey" style="width:60%">
                                <?php
                                    $infoHandle = fopen("links/components/buy/$row[3].csv", "r");
                                if (false == $infoHandle) {
                                     var_dump("links/components/buy/$row[3].csv");
                                }
                                    $line = fgetcsv($infoHandle);
                                while ($line = fgetcsv($infoHandle)) {
                                    if (isset($line[1])) {
                                        echo '<a target="_blank" href="'.sanitize($line[1]).'">'.sanitize($line[0]).'</a> ';
                                    }
                                }
    
                                    fclose($infoHandle);
                                ?></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="flex-container border">
                                <div class="w3-padding-small w3-dark-grey" style="width:40%">Info</div>
                                <div class="w3-padding-small w3-light-grey" style="width:60%">
                                    <?php

                                    $infoHandle = fopen("links/components/info/$row[3].csv", "r");
                                    $line = fgetcsv($infoHandle);
                                    while ($line = fgetcsv($infoHandle)) {
                                        if (isset($line[1])) {
                                            echo '<a target="_blank" href="'.sanitize($line[1]).'">'.sanitize($line[0]).'</a><br>';
                                        }
                                    }
    
                                    fclose($infoHandle);
                                    ?>
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <?php
        }
        fclose($handle)
        ?>
        <br>
        <div class="w3-center">
            <a href=""data:application/csv;base64,<?php echo base64_encode(file_get_contents($boardBom))?>"" download='bom.csv' class="w3-button w3-black">Download CSV BOM file</a>
        </div>
    </div>
    <!-- Third Parallax Image with Portfolio Text -->
    <div class="bgimg-4 w3-display-container w3-opacity-min">
        <div class="w3-display-middle">
            <span class="w3-xxlarge w3-text-white w3-wide">HOW TO START</span>
        </div>
    </div>
    <!-- Container (Contact Section) -->
    <div class="w3-content w3-container w3-padding-64" id="howToStart">
        <h3 class="w3-center">Follow these steps</h3>
        <div class="media">
            <img class="mr-3" style="height: 64px;width: 64px" src="<?php echo "data:image/png;base64," . base64_encode(file_get_contents("images/1.png")); ?>" alt="Generic placeholder image">
            <div class="media-body">
                <h5 class="mt-0">Get PCB data</h5>
                <p>Download PCB manufacturing file AKA CAM files collection in compressed file</p>
                <a download='cam.rar' href="data:application/rar;base64,<?php echo base64_encode(file_get_contents($camRar))?>" >Download PCB manufacturing file</a>
            </div>
        </div>
        <br>
        <div class="media">
            <img class="mr-3" style="height: 64px;width: 64px" src="<?php echo "data:image/png;base64," . base64_encode(file_get_contents("images/2.png")); ?>" alt="Generic placeholder image">
            <div class="media-body">
                <h5 class="mt-0">Manufacture a PCB</h5>
                <p>Send the the manufacturing file to a PCB manufacturer of your choice</p>
                <p>Recommend manufacturers:</p>
                <a href="http://www.allpcb.com/">AllPCB</a><br>
                <a href="https://www.seeedstudio.com/fusion.html">Seeed</a>
            </div>
        </div>
        <br>
        <div class="media">
            <img class="mr-3" style="height: 64px;width: 64px" src="<?php echo "data:image/png;base64," . base64_encode(file_get_contents("images/3.png")); ?>" alt="Generic placeholder image">
            <div class="media-body">
                <h5 class="mt-0">Get your parts</h5>
                <p>Order the board part from you favorite retailer, use the part in the BOM section, or download a CSV <a href="https://bitbucket.org/yocote/supply-card/raw/5bf095e992c2bca458f316a06f1fb64a53618061/supplyCardBOM.csv" download>file</a></p>
                <a href="http://www.aliexpress.com/">Ali Express</a><br>
                <a href="https://www.ebay.com">EBay</a>
                
            </div>
        </div>
        <br>
        <div class="media">
            <img class="mr-3" style="height: 64px;width: 64px" src="<?php echo "data:image/png;base64," . base64_encode(file_get_contents("images/4.png")); ?>" alt="Generic placeholder image">
            <div class="media-body">
                <h5 class="mt-0">Do the work</h5>
                <p>Solder the components to the board</p>
                <a href="https://www.youtube.com/watch?v=ZwU9SqO0udU" >Video on soldering DIP/PTH and wires</a><br>
                <a href="https://www.youtube.com/watch?v=4Z1B_DbW-C0" >Video on soldring SMD with solder paste and blower</a>
            </div>
        </div>
    </div>

    <!-- Third Parallax Image with Portfolio Text -->
    <div class="bgimg-5 w3-display-container w3-opacity-min">
        <div class="w3-display-middle">
            <span class="w3-xxlarge w3-text-white w3-wide">EQUIPMENT</span>
        </div>
    </div>
    <!-- Container (Contact Section) -->
    <div class="w3-content w3-container w3-padding-64" id="equipment">
        <h3 class="w3-center">This is what you will need</h3>
        <p>Buy this from you favorite retailer, cheap devices will work just fine</p>
        <div class="row">
            <div class="col-4">
                <div class="card">
                    <img class="card-img-top" src="https://img1.banggood.com/thumb/view/upload/game/SKU015801_1.jpg" alt="Soldering iron">
                    <div class="card-body">
                        <h5 class="card-title">Soldering iron</h5>
                        <p class="card-text">You will need soldering iron to solder DIP parts (like pin headers)</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <img class="card-img-top" src="https://images-na.ssl-images-amazon.com/images/I/71SYAS0VOkL._SX355_.jpg" alt="Blower">
                    <div class="card-body">
                        <h5 class="card-title">Soldering Blower</h5>
                        <p class="card-text">You will need soldering iron to solder SMD parts (most parts are SMD)</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <img class="card-img-top" src="https://ftaelectronics.com/image/cache/catalog/Hand%20Tools/250g%20Core%201.0mm%20Diameter%20Solder%2063%2037%20Soldering%20Wire%20%284%29-1024x768_0.jpg" alt="Soldering wire">
                    <div class="card-body">
                        <h5 class="card-title">Soldering wire</h5>
                        <p class="card-text">Soldering wire is mostly for soldering DIP parts</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <img class="card-img-top" src="https://ssli.ebayimg.com/images/g/QYsAAOSwtytZo5lR/s-l1600.jpg" alt="Tin soldering  paste">
                    <div class="card-body">
                        <h5 class="card-title">Tin soldering  paste</h5>
                        <p class="card-text">Tin soldering paste is mostly for soldering SMD parts</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <img class="card-img-top" src="https://www.conrad.com/medias/global/ce/4000_4999/4400/4490/4494/800686_BB_00_FB.EPS_1000.jpg" alt="Tweezers">
                    <div class="card-body">
                        <h5 class="card-title">Tweezers</h5>
                        <p class="card-text">For placing small SMD parts</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Third Parallax Image with Portfolio Text -->
    <div class="bgimg-5 w3-display-container w3-opacity-min">
        <div class="w3-display-middle">
            <span class="w3-xxlarge w3-text-white w3-wide">CONTACT</span>
        </div>
    </div>
    <!-- Container (Contact Section) -->
    <div class="w3-content w3-container w3-padding-64" id="contact">
        <h3 class="w3-center">WHO AM I</h3>
        <p class="w3-center">My name is YOCOTE</p>
        <div class="w3-row w3-padding-32 w3-section">
            <div class="w3-col m12 w3-panel">
                <div class="w3-large w3-margin-bottom">
                    <i class="fa fa-envelope fa-fw w3-hover-text-black w3-xlarge w3-margin-right"></i> Email: yocote@yoco.co.il
                </div>
                <p>leave me a note:</p>
                <form action="/action_page.php" target="_blank">
                    <div class="w3-row-padding" style="margin:0 -16px 8px -16px">
                        <div class="w3-half">
                            <input class="w3-input w3-border" type="text" placeholder="Name" required name="Name">
                        </div>
                        <div class="w3-half">
                            <input class="w3-input w3-border" type="text" placeholder="Email" required name="Email">
                        </div>
                    </div>
                    <input class="w3-input w3-border" type="text" placeholder="Message" required name="Message">
                    <button class="w3-button w3-black w3-right w3-section" type="submit">
                        <i class="fa fa-paper-plane"></i> SEND MESSAGE
                    </button>
                </form>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <footer class="w3-center w3-black w3-padding-64 w3-opacity w3-hover-opacity-off">
        <a href="#home" class="w3-button w3-light-grey"><i class="fa fa-arrow-up w3-margin-right"></i>To the top</a>
        <p>Powered by <a title="YOCOTE" target="_blank" class="w3-hover-text-green">YOCOTE</a></p>
        <span class="w3-button w3-light-grey" onclick="onClickText(this)" data-text="<?php echo sanitize(file_get_contents($disclaimer));?>" data-headline="DISCLAIMER">Web site DISCLAIMER</span>
        <span class="w3-button w3-light-grey" onclick="onClickText(this)" data-text="<?php echo sanitize(file_get_contents($license));?>" data-headline="LICENSE" >Project LICENSE</span>
    </footer>
    <!-- Add Google Maps -->
    <script>
    function myMap() {
        myCenter = new google.maps.LatLng(41.878114, -87.629798);
        var mapOptions = {
            center: myCenter,
            zoom: 12,
            scrollwheel: false,
            draggable: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

        var marker = new google.maps.Marker({
            position: myCenter,
        });
        marker.setMap(map);
    }

    // Modal Image Gallery
    function onClick(element) {
        document.getElementById("img01").src = element.src;
        document.getElementById("modal01").style.display = "block";
        var captionText = document.getElementById("caption");
        captionText.innerHTML = element.alt;
    }
     // Modal Image Gallery
    function onClickText(element) {
        document.getElementById("headline01").innerHTML = element.dataset.headline;
        document.getElementById("modal02").style.display = "block";
        var captionText = document.getElementById("text01");
        captionText.innerHTML = element.dataset.text;
    }

    // Change style of navbar on scroll
    window.onscroll = function() { myFunction() };

    function myFunction() {
        var navbar = document.getElementById("myNavbar");
        if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
            navbar.className = "w3-bar" + " w3-card" + " w3-animate-top" + " w3-white";
        } else {
            navbar.className = navbar.className.replace(" w3-card w3-animate-top w3-white", "");
        }
    }

    // Used to toggle the menu on small screens when clicking on the menu button
    function toggleFunction() {
        var x = document.getElementById("navDemo");
        if (x.className.indexOf("w3-show") == -1) {
            x.className += " w3-show";
        } else {
            x.className = x.className.replace(" w3-show", "");
        }
    }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script>
    <!--
    To use this code on your website, get a free API key from Google.
    Read more at: https://www.w3schools.com/graphics/google_maps_basic.asp
    -->
</body>

</html>