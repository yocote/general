#!/bin/bash 
WEB_PATH="/home/yovav/boards"
# Get the submodules paths and iterate over them
boards=$(git config --file .gitmodules --get-regexp path | awk '{ print $2 }')
while read -r submodulePath; do
	htmlDir=$(basename $submodulePath)
	submoduleName=$htmlDir
	htmlPath=$WEB_PATH/$htmlDir
    echo "... $submodulePath ...$htmlDir..$htmlPath"
    rm -r $htmlPath
    mkdir -p $htmlPath
    php template.php about.txt submoduleName $submodulePath > $htmlPath/index.html
 #   cp $submodulePath/files/* $htmlPath/ 
 #   cp images/* $htmlPath/
done <<< "$boards"

